# py_ocr_frequency_extractor

Python worker to extract text recognized by [rs_text_recognition_worker](https://gitlab.com/media-cloud-ai/workers/ai/rs_text_recognition_worker).
