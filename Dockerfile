FROM registry.gitlab.com/media-cloud-ai/sdks/py_mcai_worker_sdk:2.0.0-rc2

WORKDIR /app/py_ocr_frequency_extractor

ADD . /app/py_ocr_frequency_extractor

RUN apt-get --allow-releaseinfo-change update &&\
    apt-get install -y \
    cmake \
    python3 \
    python3-dev \
    python3-pip \
    enchant-2 myspell-fr-fr && \
    pip install --upgrade setuptools pip && \
    pip install . && \
    pip install json-strong-typing


ENV AMQP_QUEUE=job_ocr_frequency_extractor
ENV PYTHONPATH ${PYTHONPATH}:/app/py_ocr_frequency_extractor
ENV PYTHON_WORKER_FILENAME=/app/py_ocr_frequency_extractor/worker.py
ENV PATH $PATH:/app/py_ocr_frequency_extractor


CMD py_mcai_worker_sdk
