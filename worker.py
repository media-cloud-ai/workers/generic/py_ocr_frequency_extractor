import json
import typing

from collections import Counter
from enchant.checker import SpellChecker


class WorkerParameters:
    lang: str
    number_of_titles: int
    destination_path: typing.Optional[str] = None
    source_path: typing.Optional[str] = None
    source_paths: typing.Optional[list[str]] = None
    requirements: typing.Optional[dict] = None

    @staticmethod
    def __display__():
        return json.dumps(
            {
                k: v
                for k, v in WorkerParameters.__dict__.items()
                if not k.startswith("__")
            }
        )


class Worker:
    def __init__(self):
        pass

    @staticmethod
    def get_parameters_type() -> typing.Type:
        return WorkerParameters

    @staticmethod
    def process(handle_callback, parameters: WorkerParameters, job_id) -> dict:
        """
        Standard worker process function.
        """

        all_text = []
        dictionary = SpellChecker(parameters.lang)

        # Case where the worker is used in "one_for_many" mode
        if parameters.source_paths is not None:
            all_text = [
                reco["text"]
                for file in parameters.source_paths
                for reco in json.load(open(file, encoding="UTF-8"))["frames"]
                if (reco["text"] and dictionary.set_text(reco["text"]) is None)
            ]

        else:
            data = json.load(open(parameters.source_path, encoding="UTF-8"))
            all_text = [
                reco["text"]
                for reco in data["frames"]
                if (reco["text"] != "" and dictionary.set_text(reco["text"]) is None)
            ]

        occurences = Counter(all_text).most_common(parameters.number_of_titles)

        with open(parameters.destination_path, "w", encoding="utf-8") as out_file:
            for title in occurences:
                json.dump(title[0], out_file)
                out_file.write("\n")
